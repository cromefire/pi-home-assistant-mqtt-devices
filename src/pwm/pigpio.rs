use super::Pwm;
use std::io::{Error, Write};
use std::path::Path;

pub struct PiGpioPwm;

impl PiGpioPwm {
    pub fn new() -> PiGpioPwm {
        PiGpioPwm {}
    }
}

impl Pwm for PiGpioPwm {
    fn set_pwm(&self, pin: u8, value: u16) -> Result<(), Box<dyn std::error::Error>> {
        if value > 255 {
            panic!("Value may not exceed 255")
        }
        gpio_pwm(pin, value as u8)?;
        Ok(())
    }

    fn max_value(&self) -> u16 {
        255
    }

    fn max_pin(&self) -> u8 {
        27
    }
}

pub fn gpio_pwm(pin: u8, value: u8) -> Result<(), Error> {
    let mut file = unix_named_pipe::open_write("/dev/pigpio")?;
    file.write_all(format!("p {} {}\n", pin, value).as_bytes())?;
    Ok(())
}

pub fn validate_pigpio() {
    if Path::new(&"/dev/pigpio").exists() {
        return;
    }
    panic!("The pigpio device (/dev/pigpio) is not available");
}
