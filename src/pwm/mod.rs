use crate::{Config, PwmModule};
use pca9685::{validate_pca9685_address, PCA9685Pwm};
use pigpio::{validate_pigpio, PiGpioPwm};
use std::error::Error;
use std::sync::{Arc, Mutex};

pub mod pca9685;
pub mod pigpio;

pub async fn load_pwm_modules(config: &Config) -> Vec<Arc<Mutex<dyn Pwm>>> {
    let mut pwm_modules: Vec<Arc<Mutex<dyn Pwm>>> = vec![];
    for pm in &config.pwm_modules {
        match pm {
            PwmModule::PiGpio => {
                validate_pigpio();
                let pi_gpio = PiGpioPwm::new();
                pwm_modules.push(Arc::new(Mutex::new(pi_gpio)));
            }
            PwmModule::PCA9685 { bus,  address } => {
                validate_pca9685_address(*address);
                let pca9685 = PCA9685Pwm::new(*bus, *address, None, None).unwrap();
                pwm_modules.push(Arc::new(Mutex::new(pca9685)));
            }
        }
    }

    pwm_modules
}

pub trait Pwm {
    fn set_pwm(&self, pin: u8, value: u16) -> Result<(), Box<dyn Error>>;

    fn max_value(&self) -> u16;

    fn max_pin(&self) -> u8;
}
