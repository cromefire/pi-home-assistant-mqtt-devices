use crate::devices::light::Light;
use crate::devices::switch::Switch;
use crate::hass::switches::register_switch;
use crate::lights::update_light_state;
use crate::{register_light, Config, TopicHelper};
use async_std::sync::{Arc, Mutex};
use futures::executor::ThreadPool;
use kv::Store;
use rumqttc::{AsyncClient, EventLoop, LastWill, MqttOptions, QoS, TlsConfiguration, Transport};
use rustls::cipher_suite::TLS13_CHACHA20_POLY1305_SHA256;
use rustls::kx_group::X25519;
use rustls::version::TLS13;
use rustls::{Certificate, ClientConfig, RootCertStore};
use rustls_pemfile::{read_one, Item};
use std::borrow::Borrow;
use std::collections::HashMap;
use std::fs::File;
use std::io::BufReader;
use std::path::Path;
use std::sync::Arc as SyncArc;
use std::time::Duration;

pub async fn start(config: SyncArc<Config>) -> (EventLoop, AsyncClient) {
    log::info!("Setting up connection...");
    let ca_path = Path::new("ca.crt");
    let ca_file = File::open(ca_path).expect("CA certificate not found (expected at ca.crt)");

    let ca_cert: Certificate;
    if let Some(Item::X509Certificate(cert)) =
        read_one(&mut BufReader::new(ca_file)).expect("Cannot read ca cert")
    {
        ca_cert = Certificate(cert);
    } else {
        panic!("Cant find ca certificate in ca.crt");
    }

    let mut ca_store = RootCertStore::empty();
    ca_store.add(&ca_cert).expect("Failed to register ca cert");

    let tls_config = ClientConfig::builder()
        .with_cipher_suites(&[TLS13_CHACHA20_POLY1305_SHA256])
        .with_kx_groups(&[&X25519])
        .with_protocol_versions(&[&TLS13])
        .expect("Rustls config not configured correctly")
        .with_root_certificates(ca_store)
        .with_no_client_auth();

    let transport = Transport::Tls(TlsConfiguration::Rustls(SyncArc::from(tls_config)));

    let th = TopicHelper::new(&config);

    let host = &config.connection.host.to_string();
    let mut mqttoptions = MqttOptions::new(&config.id, host, 1883);
    mqttoptions
        .set_transport(transport)
        .set_keep_alive(Duration::from_secs(5))
        .set_credentials(&config.connection.username, &config.connection.password)
        .set_last_will(LastWill::new(
            th.activity(),
            "offline",
            QoS::ExactlyOnce,
            true,
        ));

    let (client, eventloop) = AsyncClient::new(mqttoptions, 10);

    client
        .publish(th.activity(), QoS::ExactlyOnce, true, "online")
        .await
        .unwrap();

    client
        .subscribe(&config.topics.status, QoS::AtLeastOnce)
        .await
        .unwrap();

    client
        .subscribe(th.rx("light", "+"), QoS::ExactlyOnce)
        .await
        .unwrap();

    (eventloop, client)
}

pub async fn register_devices(
    config: SyncArc<Config>,
    client: Arc<Mutex<AsyncClient>>,
    store: &Store,
    lights: &HashMap<String, Box<dyn Light>>,
    switches: &HashMap<String, Box<dyn Switch>>,
) {
    let sensor_executor = ThreadPool::new().expect("Failed to create threadpool");
    let sensor_executor = Arc::new(std::sync::Mutex::new(sensor_executor));
    let bucket = store.bucket(Some("light")).unwrap();
    for (id, light) in lights {
        let light_config = light.create_light_config();
        let config = config.borrow();
        let client = client.lock().await;
        register_light(config, &client, id, light_config, light.meta()).await;
        update_light_state(config, &client, &bucket, id, light.current_state())
            .await
            .unwrap();
    }
    for (id, switch) in switches {
        let config = config.borrow();
        let client_guard = client.lock().await;
        register_switch(config, &client_guard, id, switch.meta()).await;
        switch
            .start_listener(config, sensor_executor.clone(), client.clone())
            .await;
    }
}
