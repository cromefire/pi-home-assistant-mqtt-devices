use crate::{app_info, Config};
use serde::Serialize;
use serde_repr::Serialize_repr;
use crate::devices::light::ColorMode;

pub mod lights;
pub mod switches;

pub fn get_device_info(config: &Config) -> HassDevice {
    let id = format!("PMD_{}", config.id);
    let model = app_info::hardware_model();
    HassDevice {
        identifiers: vec![id],
        model,
        manufacturer: "PMD Contributors",
        name: &config.name,
        sw_version: app_info::VERSION,
    }
}

#[derive(Debug, PartialEq, Serialize)]
pub struct HassRegistration<'a> {
    #[serde(rename = "~")]
    prefix: &'a str,
    name: &'a str,
    unique_id: &'a str,
    #[serde(skip_serializing_if = "Option::is_none")]
    schema: Option<&'a str>,
    state_topic: &'a str,
    #[serde(skip_serializing_if = "Option::is_none")]
    payload_on: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    payload_off: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    command_topic: Option<&'a str>,
    availability_topic: &'a str,
    #[serde(skip_serializing_if = "Option::is_none")]
    brightness: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    color_mode: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    supported_color_modes: Option<Vec<ColorMode>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    min_mireds: Option<u16>,
    #[serde(skip_serializing_if = "Option::is_none")]
    max_mireds: Option<u16>,
    qos: HassQos,
    optimistic: bool,
    device: &'a HassDevice<'a>,
}

#[derive(Debug, PartialEq, Serialize)]
pub struct HassDevice<'a> {
    identifiers: Vec<String>,
    model: String,
    manufacturer: &'a str,
    name: &'a str,
    sw_version: &'a str,
}

#[derive(Serialize_repr, PartialEq, Debug)]
#[repr(u8)]
#[allow(clippy::enum_variant_names, dead_code)]
enum HassQos {
    AtMostOnce = 0,
    AtLeastOnce = 1,
    ExactlyOnce = 2,
}
