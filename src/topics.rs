use crate::Config;

pub struct TopicHelper<'a> {
    config: &'a Config
}

impl<'a> TopicHelper<'a> {
    pub fn new(config: &'a Config) -> TopicHelper {
        TopicHelper {
            config
        }
    }

    pub fn prefix(&self) -> String {
        return format!("{}/PMD_{}", self.config.topics.prefix, self.config.id);
    }

    pub fn activity(&self) -> String {
        return format!("{}/active", self.prefix());
    }

    pub fn discovery(&self, component: &str, device_id: &str) -> String {
        return format!(
            "{}/{}/PMD_{}/{}/config",
            self.config.topics.discovery, component, self.config.id, device_id
        );
    }

    pub fn device_prefix(&self, component: &str, device_id: &str) -> String {
        return format!("{}/{}/{}", self.prefix(), component, device_id)
    }

    pub fn rx(&self, component: &str, device_id: &str) -> String {
        return format!("{}/set", self.device_prefix(component, device_id))
    }

    pub fn tx(&self, component: &str, device_id: &str) -> String {
        return format!("{}/status", self.device_prefix(component, device_id))
    }
}
