use super::LightState;
use super::{Light, PwmChannel};
use crate::config::DeviceMetadata;
use crate::hass::lights::HassLightDimmableCmd;
use crate::lights::HassLightConfig;
use async_trait::async_trait;
use std::error::Error;
use std::sync::{Arc, Mutex};
use thiserror::Error;

pub struct DimmableLight {
    id: String,
    channel: Arc<Mutex<PwmChannel>>,
    state: LightState,
    meta: DeviceMetadata,
}

impl DimmableLight {
    pub fn new(
        id: String,
        channel: PwmChannel,
        state: LightState,
        meta: DeviceMetadata,
    ) -> DimmableLight {
        DimmableLight {
            id,
            channel: Arc::new(Mutex::new(channel)),
            state,
            meta,
        }
    }

    fn set_channel(&self, value: f32) -> Result<(), Box<dyn Error>> {
        let channel = self.channel.lock().unwrap();
        channel.set_value(value)
    }

    fn set_brightness(&self, brightness: u8) -> Result<(), Box<dyn Error>> {
        self.set_channel(get_channel_value(brightness))
    }
}

#[async_trait]
impl Light for DimmableLight {
    fn id(&self) -> &str {
        &self.id
    }

    fn current_state(&self) -> LightState {
        self.state.clone()
    }

    fn meta(&self) -> &DeviceMetadata {
        &self.meta
    }

    async fn apply_initial_state(&mut self) -> Result<(), Box<dyn Error>> {
        match self.state {
            LightState::Off(_) => {
                self.set_channel(0f32)?;
            }
            LightState::Brightness(state) => {
                self.set_brightness(state)?;
            }
            _ => {
                return Err(Box::new(DimmableLightError::InvalidCurrentState));
            }
        }
        Ok(())
    }

    async fn apply_new_state(&mut self, json: String) -> Result<(), Box<dyn Error>> {
        let cmd = serde_json::from_str::<HassLightDimmableCmd>(&json).unwrap();
        log::debug!("Applying new state for dimmable light: {:?}", cmd);

        let state: bool = cmd.state.into();

        if !state {
            if let LightState::Off(_) = self.state {
                return Ok(());
            }

            self.set_channel(0f32)?;
            self.state = LightState::Off(Some(Box::from(self.state.clone())))
        } else if let Some(brightness) = cmd.brightness {
            self.set_brightness(brightness)?;
            self.state = LightState::Brightness(brightness);
        } else if let LightState::Off(last_state) = &self.state {
            if let Some(last_state) = last_state {
                let last_state: LightState = *last_state.clone();
                if let LightState::Brightness(brightness) = last_state {
                    self.set_brightness(brightness)?;
                    self.state = last_state;
                } else {
                    return Err(Box::from(DimmableLightError::InvalidLastState));
                }
            } else {
                self.set_brightness(100)?;
                self.state = LightState::Brightness(100);
            }
        }

        Ok(())
    }

    fn create_light_config(&self) -> HassLightConfig {
        HassLightConfig {
            brightness: true,
            color_mode: false,
            supported_color_modes: None,
            min_mireds: None,
            max_mireds: None,
        }
    }
}

unsafe impl Sync for DimmableLight {}

unsafe impl Send for DimmableLight {}

fn get_channel_value(brightness: u8) -> f32 {
    brightness as f32 / 100_f32
}

#[derive(Error, Debug)]
pub enum DimmableLightError {
    #[error("invalid last state before off")]
    InvalidLastState,
    #[error("invalid current state")]
    InvalidCurrentState,
}
