use crate::config::{DeviceMetadata, Lights, PwmLightChannelConfig};
use crate::lights::{get_initial_light_state, HassLightConfig};
use crate::{Config, Pwm};
use async_trait::async_trait;
use dimmable::DimmableLight;
use kv::{Msgpack, Store};
use meval::Expr;
use rgb::RgbLight;
use rgb_cct::RgbCctLight;
use rgbw::RgbwLight;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::error::Error;
use std::sync::{Arc, Mutex};

mod dimmable;
mod rgb;
mod rgb_cct;
mod rgbw;

pub async fn load_lights(
    config: &Config,
    store: &Store,
    pwm_modules: &[Arc<Mutex<dyn Pwm>>],
) -> HashMap<String, Box<dyn Light>> {
    let bucket = store
        .bucket::<String, Msgpack<LightState>>(Some("light"))
        .unwrap();
    let mut light_devices: HashMap<String, Box<dyn Light>> = HashMap::new();
    for (id, light) in config.devices.lights.iter() {
        let initial_state = get_initial_light_state(id, &bucket).unwrap();
        log::debug!("Loaded initial state for light {}: {:?}", id, initial_state);

        match light {
            Lights::Dimmable {
                channel: channel_cfg,
                meta,
            } => {
                let channel = create_pwm_channel(channel_cfg, pwm_modules).await;
                let light = DimmableLight::new(id.clone(), channel, initial_state, meta.clone());
                light_devices.insert(id.clone(), Box::new(light));
            }
            Lights::Rgb {
                red,
                green,
                blue,
                meta,
            } => {
                let red_channel = create_pwm_channel(red, pwm_modules).await;
                let green_channel = create_pwm_channel(green, pwm_modules).await;
                let blue_channel = create_pwm_channel(blue, pwm_modules).await;
                let mut light = RgbLight::new(
                    id.clone(),
                    red_channel,
                    green_channel,
                    blue_channel,
                    initial_state,
                    meta.clone(),
                );
                light.apply_initial_state().await.unwrap();
                light_devices.insert(id.clone(), Box::new(light));
            }
            Lights::Rgbw {
                red,
                green,
                blue,
                white,
                meta,
            } => {
                let red_channel = create_pwm_channel(red, pwm_modules).await;
                let green_channel = create_pwm_channel(green, pwm_modules).await;
                let blue_channel = create_pwm_channel(blue, pwm_modules).await;
                let white_channel = create_pwm_channel(white, pwm_modules).await;
                let mut light = RgbwLight::new(
                    id.clone(),
                    red_channel,
                    green_channel,
                    blue_channel,
                    white_channel,
                    initial_state,
                    meta.clone(),
                );
                light.apply_initial_state().await.unwrap();
                light_devices.insert(id.clone(), Box::new(light));
            }
            Lights::RgbCct {
                red,
                green,
                blue,
                cold,
                warm,
                color_temp_config,
                meta,
            } => {
                let red_channel = create_pwm_channel(red, pwm_modules).await;
                let green_channel = create_pwm_channel(green, pwm_modules).await;
                let blue_channel = create_pwm_channel(blue, pwm_modules).await;
                let cold_channel = create_pwm_channel(cold, pwm_modules).await;
                let warm_channel = create_pwm_channel(warm, pwm_modules).await;
                let mut light = RgbCctLight::new(
                    id.clone(),
                    red_channel,
                    green_channel,
                    blue_channel,
                    cold_channel,
                    warm_channel,
                    initial_state,
                    color_temp_config.clone(),
                    meta.clone(),
                );
                light.apply_initial_state().await.unwrap();
                light_devices.insert(id.clone(), Box::new(light));
            }
            _ => {
                todo!()
            }
        }
    }
    light_devices
}

async fn create_pwm_channel(
    config: &PwmLightChannelConfig,
    pwm_modules: &[Arc<Mutex<dyn Pwm>>],
) -> PwmChannel {
    let pwm = pwm_modules
        .get(config.output as usize)
        .unwrap_or_else(|| panic!("PWM module {} not found", config.output));
    let pwm_guard = pwm.lock().unwrap();
    if pwm_guard.max_pin() < config.pin {
        panic!("PWM module {} has no pin {}", config.output, config.pin)
    }
    let expr_opt: Option<Result<Expr, _>> = config.curve.clone().map(|str| str.parse());
    let expr = if let Some(Ok(ex)) = expr_opt {
        Some(ex)
    } else if let Some(Err(err)) = expr_opt {
        panic!("Failed to parse curve: {}", err)
    } else {
        None
    };
    let curve = expr.map(|ex| -> Box<dyn Fn(f64) -> f64> {
        let bound_res = ex.bind("x");
        if let Err(err) = bound_res {
            panic!("Failed to bind to 'x' in curve: {}", err);
        } else {
            Box::new(bound_res.unwrap())
        }
    });
    PwmChannel::new(pwm.clone(), config.pin, config.min, config.max, curve)
}

#[async_trait]
pub trait Light {
    fn id(&self) -> &str;
    fn current_state(&self) -> LightState;
    fn meta(&self) -> &DeviceMetadata;
    async fn apply_initial_state(&mut self) -> Result<(), Box<dyn Error>>;
    async fn apply_new_state(&mut self, json: String) -> Result<(), Box<dyn Error>>;
    fn create_light_config(&self) -> HassLightConfig;
}

pub struct PwmChannel {
    pwm: Arc<Mutex<dyn Pwm>>,
    pin: u8,
    base: u16,
    span: f64,
    curve: Option<Arc<Mutex<Box<dyn Fn(f64) -> f64>>>>,
}

impl PwmChannel {
    pub fn new(
        pwm: Arc<Mutex<dyn Pwm>>,
        pin: u8,
        min: u16,
        max: u16,
        curve: Option<Box<dyn Fn(f64) -> f64>>,
    ) -> PwmChannel {
        PwmChannel {
            pwm,
            pin,
            base: min,
            span: (max - min) as f64,
            curve: curve.map(|c| Arc::new(Mutex::new(c))),
        }
    }

    fn set_pwm(&self, pin: u8, value: u16) -> Result<(), Box<dyn Error>> {
        let pwm = self.pwm.lock().unwrap();
        pwm.set_pwm(pin, value)
    }
}

impl PwmChannel {
    pub fn set_value(&self, perc: f32) -> Result<(), Box<dyn Error>> {
        assert!(perc <= 1f32, "Percentage can be at maximum 1.0: {}", perc);
        let mapped_perc = if let Some(curve) = &self.curve {
            let curve = curve.lock().unwrap();
            let mv = curve(perc as f64);
            assert!(mv <= 1f64, "Percentage can be at maximum 1.0: {}", perc);
            mv
        } else {
            perc as f64
        };
        if mapped_perc < 0.01f64 {
            self.set_pwm(self.pin, 0)
        } else {
            let value = self.base + (mapped_perc * self.span) as u16;
            log::debug!("Setting PWM pin {} to {}", self.pin, value);
            self.set_pwm(self.pin, value)
        }
    }
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct CctState {
    pub brightness: u8,
    pub color_temp: u16,
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct RgbState {
    pub brightness: u8,
    pub red: u8,
    pub green: u8,
    pub blue: u8,
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct RgbwState {
    pub brightness: u8,
    pub red: u8,
    pub green: u8,
    pub blue: u8,
    pub white: u8,
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct RgbwwState {
    pub brightness: u8,
    pub red: u8,
    pub green: u8,
    pub blue: u8,
    pub cold: u8,
    pub warm: u8,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum LightState {
    Off(Option<Box<LightState>>),
    On,
    Brightness(u8),
    ColorTemp(CctState),
    Rgb(RgbState),
    Rgbw(RgbwState),
    Rgbww(RgbwwState),
}

#[derive(Debug, PartialEq, Serialize, Deserialize, Clone)]
pub enum ColorMode {
    #[serde(rename = "onoff")]
    OnOff,
    #[serde(rename = "brightness")]
    Brightness,
    #[serde(rename = "color_temp")]
    ColorTemp,
    #[serde(rename = "rgb")]
    Rgb,
    #[serde(rename = "rgbw")]
    Rgbw,
    #[serde(rename = "rgbww")]
    Rgbww,
}

pub fn get_channel_brightness(color_value: u8, brightness: u8) -> f32 {
    let color_fraction = color_value as f32 / 255_f32;
    let brightness_fraction = brightness as f32 / 255_f32;

    color_fraction * brightness_fraction
}

pub fn get_brightness_from_state(state: LightState) -> u8 {
    match state {
        LightState::Off(last_state) => {
            if let Some(last_state) = last_state {
                if let LightState::Off(_) = *last_state {
                    panic!("Off state can't contain Off as last state");
                }
                get_brightness_from_state(*last_state)
            } else {
                0_u8
            }
        }
        LightState::On => 100_u8,
        LightState::Brightness(brightness) => brightness,
        LightState::ColorTemp(state) => state.brightness,
        LightState::Rgb(state) => state.brightness,
        LightState::Rgbw(state) => state.brightness,
        LightState::Rgbww(state) => state.brightness,
    }
}
