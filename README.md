# Pi Home Assistant Devices
A daemon to connect devices to home assistant via low level interfaces.

## Hard and Software support
### Supported Interfaces / Services
- pigpio (Digital input)
- PCA9685 (PWM output)
- MCP300X (Analog inputs)

### Supported Devices
#### Via Digital Inputs
- Switches (ON/OFF)

#### Via Analog Inputs
- Switches (ON/OFF with threshold)

#### Via Analog/PWM Outputs
- LEDs (ON/OFF, Dimmable, RGB, RGBW, RGB-CCT)

### Supported Pis
Most Pi and Pi-like devices should work or can be made working easily

Tested:
- Raspberry Pi Zero 1
- Rock Pi S

## Configuration
**TODO**
